<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_model extends CI_Model {

	//funcion constructora
	public function __construct(){
		parent::__construct();
    }
	//funcion que autentifica un usuario, verifica que se encuentre en la base de datos
	public function login($nombre_autor,$password_autor){
		//Nos devuelve una fila es por que existe
        $this->db->where('nombre_autor',$nombre_autor);
        $this->db->where('password_autor',$password_autor);
        $resultado = $this->db->get('autores');
        if($resultado->num_rows()>0){
            return true;
        }else{
            return false;
        }
	}
}
