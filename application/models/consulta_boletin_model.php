<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class consulta_boletin_model extends CI_Model {

    public function consulta_boletin($numero){
        return $this->db->query("SELECT boletines.titulo_boletin,boletines.md_boletin,boletines.referencia_boletin 
                                        FROM boletines WHERE boletines.id_boletin=$numero")->result();
    }
}