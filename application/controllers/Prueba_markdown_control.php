<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prueba_markdown_control extends CI_Controller {

	public function index(){
        
    }

    public function Boletin_Markdown_2HTML($numero){
        $this->load->library('markdown');
        $this->load->model('consulta_boletin_model');
        $boletines = $this->consulta_boletin_model->consulta_boletin($numero);
        foreach($boletines as $boletin){
            $md_boletin = $boletin->md_boletin;
            $referencia_boletin = $boletin->referencia_boletin;
        }
        echo $this->markdown->parse($md_boletin);
    }
}