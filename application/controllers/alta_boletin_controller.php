<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class alta_boletin_controller extends CI_Controller{
    
    public function index(){
        $this->load->model('alta_boletin_model');
        $this->alta_boletin_model->alta_boletin();
        $this->load->view('alta_boletin_view');
    }
}