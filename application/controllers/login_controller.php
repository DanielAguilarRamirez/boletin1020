<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_controller extends CI_Controller {

	//funcion constructora
	public function __construct(){
		parent::__construct();
	}

	//funcion principal
	public function index(){

		if($this->session->userdata('nombre_autor')){
			redirect('welcome');
		}

		if(isset($_POST['password_autor'])){
			$this->load->model('login_model');
			if($this->login_model->login($_POST['nombre_autor'],md5($_POST['password_autor']))){
				redirect('welcome');
				//inicio de session
				$this->session->set_userdata('nombre_autor',$_POST['nombre_autor']);
			}else{
				redirect('login_controller');
				//redirect('welcome');
			}
		}
		//cargamos la vista login.php
		$this->load->view('login_view');
	}

	//
	public function logout(){
		$this->session->sess_destroy();
		redirect('login_controller');
	}
	//prueba de conexion a la base de datos
	public function prueba_conexion_bd(){
		print_r($this->db);
	}
}
