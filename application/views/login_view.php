<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login al Sistema</title>
</head>
<body>
    <h1>Login Boletin 1020</h1>
    <p>Escriba su nombre de usuario y contraseña.</p>
    <form action="" method="post" name="login" id="login">
        <label for="nombre_autor">Usuario</label>
        <br>
        <input type="text" name="nombre_autor" id="nombre_autor" required placeholder="User name" autofocus>
        <br>
        <label for="password_autor">Contraseña</label>
        <br>
        <input type="password" name="password_autor" id="password_autor" required placeholder="Password" autofocus>
        <br>
        <br>
        <input type="submit" value="Acceder al Sistema">
        <br>
        <br>
        <a href="">¿Olvido su contraseña?</a>
    </form>
</body>
</html>
